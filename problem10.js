/**
Summation of primes
Submit

 Show HTML problem content 
Problem 10
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.
**/

function isPrime(possiblePrime, primesList){
	var prime=true;
	
	if(primesList.length == 0){
		for (var i =possiblePrime-1; i >1; i--) {
			if(possiblePrime %i==0){
				prime=false;
				break;
			}
		}
	} else {
		for(var i = 0; i< primesList.length; i++){
			var currentPrime = primesList[i];
			if(possiblePrime%currentPrime == 0){
				prime = false;
				break;
			}
		}
	}
	
	return prime;
}


function primeAdd(maximum){
	var primes = [];
	var total=0;
	var i=1
	while(i<maximum){
		i++
		if(isPrime(i, primes)){
			primes.push(i)
			total+=i;
		}
	}

	return total;
}
console.log(primeAdd(200000));