/**
10001st prime
Submit

 Show HTML problem content 
Problem 7
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?
**/

function isPrime(possiblePrime){
	var prime=true
	for(var i=possiblePrime-1; i>1; i--){
		if(possiblePrime %i==0){
			prime=false;
			break;
		}
	}
	return prime
}
function findNPrime(n){
	var primesFound=0
	var current=1

	while(primesFound < n){
		current++
		if(isPrime(current)){
			primesFound++
		}

	}
	return current
}
console.log(findNPrime(10001));