/**
Multiples of 3 and 5
Show HTML problem content 
Problem 1
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
**/


var sumMultiplesOf3Or5=function(target){

	var total=0
	for(var i=1; i<target;i++){
		if(i%3==0 || i%5==0){
			//console.log(i);
			total=total+i;
		}
	}
	return total;
};


var test=sumMultiplesOf3Or5(10);

console.log(test);

var test2=sumMultiplesOf3Or5(1000);

console.log(test2);